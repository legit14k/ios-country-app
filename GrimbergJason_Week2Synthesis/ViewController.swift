//
//  ViewController.swift
//  GrimbergJason_Week2Synthesis
//
//  Created by Jason Grimberg on 6/11/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.

import UIKit

class ViewController: UIViewController {

    // Outlets
    @IBOutlet weak var mainLabel: UILabel!
    
    // Variables to hold total countries and population
    var totalCountries: Int = 0
    var totalPopulation: Int = 0
    var selectedContinent: String?
    
    
    // Dictionary to hold the values of each continent
    var worldDictionary = [String: [String: Int]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add all continents to the dictionary
        worldDictionary["Africa"] = [:]
        worldDictionary["Asia"] = [:]
        worldDictionary["Australia"] = [:]
        worldDictionary["Europe"] = [:]
        worldDictionary["North America"] = [:]
        worldDictionary["South America"] = [:]
        
        // Set the main label to the total countries and the total population
        mainLabel.text = "There are \(totalCountries) countries in the world with a total population of \(totalPopulation)."
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Set the new destination to the second view controller
        let destination = segue.destination as! ContinentViewController
        
        // Set the continent name to the selected one
        destination.continentName = selectedContinent
        
        // Hold the temp value to send over
        var tempPop:Int = 0
        
        // Loop through the selected dictionary for all of the populations
        for population in worldDictionary[selectedContinent!]! {
            
            // Add up all populations
            tempPop = population.value + tempPop
        }
        
        // Set the destination population to the added value
        destination.numberOfPopulation = tempPop
        
        // Send over the dictionary to the second view controller
        destination.continentDictionary = worldDictionary[selectedContinent!]!
    }
    @IBAction func goToContinent(_ sender: UIButton) {
        
        // Switch statment to set the selected continent
        switch sender.tag {
            
        case 0:
        selectedContinent = "Africa"
            performSegue(withIdentifier: "toContinentView", sender: sender)
        case 1:
        selectedContinent = "Asia"
            performSegue(withIdentifier: "toContinentView", sender: sender)
        case 2:
        selectedContinent = "Australia"
            performSegue(withIdentifier: "toContinentView", sender: sender)
        case 3:
        selectedContinent = "Europe"
            performSegue(withIdentifier: "toContinentView", sender: sender)
        case 4:
        selectedContinent = "North America"
            performSegue(withIdentifier: "toContinentView", sender: sender)
        case 5:
        selectedContinent = "South America"
            performSegue(withIdentifier: "toContinentView", sender: sender)
        default:
        selectedContinent = "No Continent Selected."
        
        }
    }
    
    // Unwind to the root with the continent name view as the source
    @IBAction func unwindToRoot(segue: UIStoryboardSegue){
        
        // Setting the source as the continent view controller
        let source = segue.source as! ContinentViewController
        
        // Update the world dictionary with the updated values from the previous screen
        worldDictionary.updateValue(source.continentDictionary, forKey: source.continentName!)
        
        var tempPopulation: Int = 0
        var tempCountries: Int = 0
        
        // Set the total number of countries and the population to the total added together
        for i in worldDictionary.keys {
            for j in worldDictionary[i]! {
                // Add the population of all values
                tempPopulation = j.value + tempPopulation
            }
            // Add all countries of all keys
            tempCountries = (worldDictionary[i]?.count)! + tempCountries
        }
        
        // Set the countries and population
        totalCountries = tempCountries
        totalPopulation = tempPopulation
        
        // Set the main label to the total countries and the total population
        mainLabel.text = "There are \(totalCountries) countries in the world with a total population of \(totalPopulation)."
    }
}


