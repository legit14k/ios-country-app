//
//  ContinentViewController.swift
//  GrimbergJason_Week2Synthesis
//
//  Created by Jason Grimberg on 6/11/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class ContinentViewController: UIViewController {

    // Outlets
    @IBOutlet weak var continentNameLabel: UILabel!
    @IBOutlet weak var continentDescription: UILabel!
    
    // Dictionary [country: population] to hold the continent's countries
    var continentDictionary = [String : Int]()
    
    // Variables to hold data coming in and going out
    var continentName: String?
    var numberOfCountries: Int = 0
    var numberOfPopulation: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the lable to the passed value
        continentNameLabel.text = continentName
        
        // Set the number of countries to the amount of items in the dictionary
        numberOfCountries = continentDictionary.count
        
        // Set the label to the passed values of name, countries, and population
        continentDescription.text = "\(continentName!) has \(numberOfCountries) countries, with a population of \(numberOfPopulation)."
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Unwind to the continent controller
    @IBAction func unwindToContinent(segue: UIStoryboardSegue) {
        let source = segue.source as! NewCountryViewController
        
        // Update the current continent dictionary with the new input
        continentDictionary[source.newCountryNameString!] = source.newCountryPopulationInt
        
        // Set the number of countries to the amount of items in the dictionary
        numberOfCountries = continentDictionary.count
        
        // Temp number to hold the number to add to the other number
        let tempPop = Int(source.newCountryPop.text!)! + numberOfPopulation
        
        // Set the temp number to the total population
        numberOfPopulation = Int(tempPop)
        
        // Print out the label again
        continentDescription.text = "\(continentName!) has \(numberOfCountries) countries, with a population of \(numberOfPopulation)."
    }
}
