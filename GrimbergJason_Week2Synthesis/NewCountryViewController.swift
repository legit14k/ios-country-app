//
//  NewCountryViewController.swift
//  GrimbergJason_Week2Synthesis
//
//  Created by Jason Grimberg on 6/11/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class NewCountryViewController: UIViewController {

    // Outlets
    @IBOutlet weak var newCountryName: UITextField!
    @IBOutlet weak var newCountryPop: UITextField!
    
    // Variables
    var newCountryPopulationInt: Int?
    var newCountryNameString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Go back button - no information returned
    @IBAction func goBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Function to check if the population is an int
    func isStringAnInt(string: String) -> Bool {
        return Int(string) != nil
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        // Check if either of the text fields are empty
        if (newCountryName.text?.isEmpty)! || (newCountryPop.text?.isEmpty)! {
            
            // Set the alert message
            let alert = UIAlertController(title: "Warning", message: "You must fill out all input fields", preferredStyle: UIAlertControllerStyle.alert)
            
            // Set the OK button
            let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            
            // Add the OK button to the alert
            alert.addAction(okButton)
            
            // Present the alert to the user
            present(alert, animated: true, completion: nil)
            
            // Return false if either of the text fields are empty
            return false
        }
        
        if (isStringAnInt(string: newCountryPop.text!)) {
            // Set the variables to the text fields
            newCountryNameString = newCountryName.text
            newCountryPopulationInt = Int(newCountryPop.text!)
            
            // Only then can you return true
            return true
        }
        
        // Return false if reached
        return false
    }
}
